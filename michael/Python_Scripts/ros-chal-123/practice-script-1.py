sample_sequence1 = ''
#sample_sequence = sample_sequence1.upper()
sample_sequence = list(sample_sequence1)
acceptable_nucloetides = ['A','T','C','G']
As = 0 
Ts = 0 
Gs = 0 
Cs = 0 
def individual_nucleotide_display(sample_sequence):
    '''Sample sequence is a list of nucleotides'''
    acceptable_nucloetides = ['A','T','C','G']
    for i in range(0,len(sample_sequence)):
        if sample_sequence[i] not in acceptable_nucloetides:
            print('Error, fix  '+ sample_sequence[i] + ' at position ' + str(i))
            break
        else:
            print(sample_sequence[i] + ' ' + str(i))

def dna_sequence_check(sample_sequence):
    '''Function that checks if a inserted sequence is acceptable to work with. '''
    acceptable_nucloetides = ['A','T','C','G']
    error_list = []
    for i in range(0,len(sample_sequence)):
        if sample_sequence[i] not in acceptable_nucloetides:
            error_list.append('Nucleotide Error, fix  '+ sample_sequence[i] + ' at position ' + str(i))
        elif sample_sequence[i].islower == False:
            error_list.append('Case Error ' + sample_sequence[i] + ' ' + str(i))
    if len(error_list) == 0:
        print('Good Sequence')
    else:
        print(error_list)

def gc_content(count_a,count_t,count_g, count_c, decimals = 4):
    return round(((count_g+count_c)/(count_g+count_c+count_a+count_t)) * 100, decimals)

def nucleotide_count(sample_sequence):
    As = 0 
    Ts = 0 
    Gs = 0 
    Cs = 0 
    for i in range(0,len(sample_sequence)):
        if sample_sequence[i] == 'A':
            As += 1
        elif sample_sequence[i] == 'T':
            Ts += 1
        elif sample_sequence[i] == 'G':
            Gs += 1
        elif sample_sequence[i] == 'C':
            Cs += 1
        else:
            print('error')
            break
    gc = gc_content(As,Ts,Gs,Cs)
    print('A: ' + str(As))
    print('C: ' + str(Cs))
    print('G: ' + str(Gs))
    print('T: ' + str(Ts))
    print('GC Content: %' + str(gc))

def rosalind_nucleotide_count(sample_sequence):
    As = 0 
    Ts = 0 
    Gs = 0 
    Cs = 0 
    for i in range(0,len(sample_sequence)):
        if sample_sequence[i] == 'A':
            As += 1
        elif sample_sequence[i] == 'T':
            Ts += 1
        elif sample_sequence[i] == 'G':
            Gs += 1
        elif sample_sequence[i] == 'C':
            Cs += 1
        else:
            print('error')
            break
    #gc = gc_content(As,Ts,Gs,Cs)
    print(As,Cs,Gs,Ts)
               



#dna_sequence_check(sample_sequence)
#rosalind_nucleotide_count(sample_sequence)
#reversed_seq = list(reversed(sample_sequence))
#print(reversed_seq)

