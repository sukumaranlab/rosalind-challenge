class Mendel(object):
    def __init__(self, haplotype1, haplotype2):
        self.haplotype1 = haplotype1
        self.haplotype2 = haplotype2
        self.LOWERCASE = "abcdefghijklmnopqrstuvwxyz"

    def __allele_separation(self, haplotype_input):
        gene_list = []
        for x in range(len(haplotype_input)):
            if x %2 == 0:
                allele_list = [haplotype_input[x], haplotype_input[x+ 1]]
                gene_list.append(allele_list)
            elif x %2 != 0:
                pass
        return(gene_list)

    def __allele_combination(self, haplotype_input, index, subindex, gene_combination, passed_string):
            passed_string = passed_string + haplotype_input[index][subindex]
            self.__allele_combination_rec(haplotype_input, index, gene_combination, passed_string)

    def __allele_combination_rec(self, haplotype_input, index = -1, gene_combination = None, passed_string = ""): 
        if gene_combination == None:
            gene_combination = []

        if index == (len(haplotype_input) - 1):
            gene_combination.append(passed_string)
        else:
            self.__allele_combination(haplotype_input, index + 1, 0, gene_combination, passed_string)
            self.__allele_combination(haplotype_input, index + 1, 1, gene_combination, passed_string)

            return(gene_combination)

    def __allele_mixing(self, haplotype_input_1, haplotype_input_2):
        all_possibilities = []
        for index_1 in range(len(haplotype_input_1)):
            for index_2 in range(len(haplotype_input_2)):
                gene_possibility = ""
                for item1, item2 in zip(haplotype_input_1[index_1], haplotype_input_2[index_2]):
                    if item1 in self.LOWERCASE:
                        gene_possibility = gene_possibility + item1
                    if item2 in self.LOWERCASE:
                        gene_possibility = gene_possibility + item2
                    if item1 not in self.LOWERCASE:
                        gene_possibility = gene_possibility + item1
                    if item2 not in self.LOWERCASE:
                        gene_possibility = gene_possibility + item2
                all_possibilities.append(gene_possibility)
        return(all_possibilities)

    def __allele_probabilities(self, results):
        percentage = 1 / len(results)
        punnet_dict = {}

        for elem in results:
            if elem not in punnet_dict.keys():
                punnet_dict[elem] = percentage
            elif elem in punnet_dict.keys():
                punnet_dict[elem] += percentage

        return(punnet_dict)


    def punnet_square(self):
        print("Creating your Punnet square...")
        return(
         self.__allele_probabilities(
          self.__allele_mixing(
           self.__allele_combination_rec(
            self.__allele_separation(self.haplotype1)
        ),
           self.__allele_combination_rec(
            self.__allele_separation(self.haplotype2)
        ))))
        



if __name__ == "__main__":
    test = Mendel("TTEESSTT", "tteesstt")
    test = test.punnet_square()
    print(test)
        
