import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    print("Loading word list from file...")
    inFile = open(WORDLIST_FILENAME, 'r')
    line = inFile.readline()
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def choose_word(wordlist):
    return random.choice(wordlist)

wordlist = load_words()

def is_word_guessed(secret_word, letters_guessed):
  length = len(secret_word)
  correct = 0
  for letter in secret_word:
      if letter in letters_guessed:
          correct +=1
      if correct == length:
          return(True)

  return(False)


def get_guessed_word(secret_word, letters_guessed):
  hangman_list = []
  hangman = ""
  for letter in secret_word:
    if letter in letters_guessed:
      hangman_list.append(letter)
    else:
      hangman_list.append("_")
  hangman = ' '.join(str(elem) for elem in hangman_list)
  return(hangman)


def get_available_letters(letters_guessed):
    alphabet = ('abcdefghijklmnopqrstuvwxyz')
    remaining_list = []
    for char in alphabet:
        if char in letters_guessed:
            pass
        else:
            remaining_list.append(char)
    remaining_string = ' '.join(str(elem) for elem in remaining_list)
    return(remaining_string)  


def hangman(secret_word):
    warnings = 3
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    num_guesses = 6
    print("You start the game with ", warnings, "warnings and ", num_guesses, "guesses.")

    while True:
        available_letters = get_available_letters(letters_guessed)
        guessed_word = get_guessed_word(secret_word, letters_guessed)
        result = is_word_guessed(secret_word,letters_guessed)

        if num_guesses == 0:
            print("YOU LOSE! YOU RAN OUT OF GUESSES! The secret word was:", secret_word)            
            break
        elif warnings == 0:
            print("YOU LOSE! YOU RAN OUT OF WARNINGS! The secret word was:", secret_word)
            break 
        elif result == True:
            print("OMG YAY YOU WON! The secret word was: ", secret_word )
            break


        print(" I am thinking of a word that is", len(secret_word), "letters long...", 
        "\n", guessed_word,
        "\n", "You have ", num_guesses, " guesses remaining...",
        "\n", "these are the available letters:", available_letters)
        guess = input("Make a guess: ")

        if len(guess) > 1:
            warnings -= 1
            print("----------------------------------------------------------\nPLEASE ONLY INPUT A SINGLE LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess == "":
          print("----------------------------------------------------------")
          pass
        elif guess not in alphabet:
            warnings -= 1
            print("----------------------------------------------------------\nPLEASE ONLY INPUT A VALID LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess in letters_guessed:
            warnings -= 1
            print("----------------------------------------------------------\nYOU ALREADY GUESSED THAT LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess not in secret_word:
            num_guesses -= 1
            print("----------------------------------------------------------\nWHOOPS! SEEMS LIKE THAT LETTER WAS NOT IN THE WORD!")
        elif guess in secret_word:
            print("----------------------------------------------------------\nYAY! THAT LETTER WAS FOUND IN THE WORD!")
        
        letters_guessed.append(guess)


# -----------------------------------


def match_with_gaps(my_word, other_word):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    other_word_letters = list(other_word)
    my_word_letters = []
    letters_checked = []

    for char in my_word:
        if char in alphabet or char == "_":
            my_word_letters.append(char)
        
        if char in alphabet and char not in letters_checked:
            letters_checked.append(char)

    if len(my_word_letters) != len(other_word_letters):
        return(False)

    for n in range(len(other_word)):
        if my_word_letters[n] != other_word_letters[n]:
            if other_word_letters[n] not in letters_checked:
                if my_word_letters[n] in letters_checked:
                    return(False)
            elif my_word_letters[n] in letters_checked or my_word_letters[n] == "_":
                return(False)

    return(True)



def show_possible_matches(my_word):
    possible_matches = []
    for word in wordlist:
        other_word = word
        if match_with_gaps(my_word, other_word) == True:
            possible_matches.append(word)
    print(possible_matches)



def hangman_with_hints(secret_word):
    warnings = 3
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    num_guesses = 6
    print("You start the game with ", warnings, "warnings and ", num_guesses, "guesses.\nIf you need help, type * as your guess.")

    while True:
        available_letters = get_available_letters(letters_guessed)
        guessed_word = get_guessed_word(secret_word, letters_guessed)
        result = is_word_guessed(secret_word,letters_guessed)
        my_word = guessed_word

        if num_guesses == 0:
            print("YOU LOSE! YOU RAN OUT OF GUESSES! The secret word was:", secret_word)            
            break
        elif warnings == 0:
            print("YOU LOSE! YOU RAN OUT OF WARNINGS! The secret word was:", secret_word)
            break 
        elif result == True:
            print("OMG YAY YOU WON! The secret word was: ", secret_word )
            break


        print(" I am thinking of a word that is", len(secret_word), "letters long...", 
        "\n", guessed_word,
        "\n", "You have ", num_guesses, " guesses remaining...",
        "\n", "these are the available letters:", available_letters)
        guess = input("Make a guess: ")
        
        if guess == "*":
            show_possible_matches(my_word)
        elif len(guess) > 1:
            warnings -= 1
            print("----------------------------------------------------------\nPLEASE ONLY INPUT A SINGLE LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess == "":
          print("----------------------------------------------------------")
          pass
        elif guess not in alphabet:
            warnings -= 1
            print("----------------------------------------------------------\nPLEASE ONLY INPUT A VALID LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess in letters_guessed:
            warnings -= 1
            print("----------------------------------------------------------\nYOU ALREADY GUESSED THAT LETTER. YOU HAVE ", warnings, " WARNINGS LEFT")
        elif guess not in secret_word:
            num_guesses -= 1
            print("----------------------------------------------------------\nWHOOPS! SEEMS LIKE THAT LETTER WAS NOT IN THE WORD!")
        elif guess in secret_word:
            print("----------------------------------------------------------\nYAY! THAT LETTER WAS FOUND IN THE WORD!")
        
        letters_guessed.append(guess)



# ----------------------------------------------------------------------


if __name__ == "__main__":
    ask = int(input("type 1 for easy mode, or 2 for hard mode: "))
    if ask == 2:
        secret_word = choose_word(wordlist)
        letters_guessed = []
        hangman(secret_word)
    elif ask == 1:
        secret_word = choose_word(wordlist)
        letters_guessed = []
        hangman_with_hints(secret_word)
    else:
        print("Input not accepted. Please run the program again.")
        pass
