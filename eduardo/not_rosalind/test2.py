def alleleCombination(haplotypeInput, index = 0, geneCombination = []):
    tempList = []

    for item in range(len(haplotypeInput)):
        if item == index:
            tempList.append(haplotypeInput[item][1])
        elif item != index:
            tempList.append(haplotype[item][0])

    combination = ''.join(str(elem) for elem in tempList)
    geneCombination.append(combination)
    
    for item in range(len(haplotypeInput[0:index])):
            tempList[item] = haplotypeInput[item][1]
            combination = ''.join(str(elem) for elem in tempList)
            geneCombination.append(combination)

    if index == (len(haplotypeInput)) - 1:
        return(geneCombination)
    else:
        return(alleleCombination(haplotypeInput, index + 1, geneCombination))

geneCombination =[]
haplotypeInput = alleleSeparation("aAbBcCdD")
def alleleCombinationRec( index = 0, passedString = ""): 
    if index == (len(haplotypeInput) -1):
        geneCombination.append(passedString+haplotypeInput[index][0])
        geneCombination.append(passedString+haplotypeInput[index][1])
    else:
        alleleCombinationRec(index + 1,  passedString+haplotypeInput[index][0])
        alleleCombinationRec(index + 1,  passedString+haplotypeInput[index][1])

