
annual_salary = float(input("what is your annual salary?"))
portion_saved = float(input("what percentage of your salary will be saved (in decimal)?"))
total_cost= float(input("how much will your dream home cost?"))
semiannual_raise = float(input("how big of a raise will you be getting every six months (in decimal)?"))

down_payment = .25*(total_cost)
monthly_savings = annual_salary/12*portion_saved
month = 0
current_savings = 0
count = 0

while (current_savings < down_payment):
    if (count > 0 and count %6 == 0):    
        monthly_savings = monthly_savings*(1+semiannual_raise)
        #monthly_savings is proportional to annual_salary
    monthly_return = current_savings*.04/12
    month = month + 1
    current_savings = current_savings  + monthly_return + monthly_savings
    count = count+1

print(month)