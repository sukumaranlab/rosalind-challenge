#annual_salary = float(input("what is your annual salary?"))
annual_salary = int(input("What's your annual salary?"))
annual_raise = .07
annual_return = .04
down_payment = 250000
years = 3

steps = 0
total_earnings = 0
savings = 0
deposit = 0

low_boundary = 0
high_boundary = 1
savings_rate = (low_boundary+high_boundary)/2
# print(low_boundary, high_boundary, savings_rate)



#while (savings != down_payment):
while (True):
    temp_annual_salary = annual_salary
    savings = 0
    for x in range(years):
        #print("start",deposit, annual_salary, savings) 
        deposit = (temp_annual_salary*savings_rate)
        temp_annual_salary = temp_annual_salary*(1+annual_raise)
        savings = (savings + deposit)*(1+annual_return)
    

    # print(savings,down_payment,abs(savings - down_payment)  )
    if (abs(savings - down_payment) <= 0.01):
        print("the best savings rate is:" + str(savings_rate))
        print("steps in bisection search:" + str(steps))
        break

    if (savings < down_payment):
        low_boundary =  (savings_rate)
        savings_rate = (low_boundary + high_boundary)/2


    if (savings > down_payment):
        high_boundary =  (savings_rate)
        savings_rate = (low_boundary + high_boundary)/2
    steps = steps + 1

    # print("rate =", savings_rate, "steps =", steps, "low =", low_boundary, "high =", high_boundary)