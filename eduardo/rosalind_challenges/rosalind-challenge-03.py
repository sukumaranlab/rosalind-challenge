DNA = ("AAAACCCGGT")

DNA_reverse = DNA[::-1]
DNA_rev_comp = str()

for char in DNA_reverse:
    if char == "A":
        DNA_rev_comp = DNA_rev_comp + "T"
    if char == "G":
        DNA_rev_comp = DNA_rev_comp + "C"
    if char == "C":
        DNA_rev_comp = DNA_rev_comp + "G"
    if char == "T":
        DNA_rev_comp = DNA_rev_comp + "A"

print(DNA_rev_comp)