months = int(input("Number of MONTHS:"))

while (months > 40 or months < 0):
    months = int(input("MONTHS must be represented by a positive integer that is not greater than 40:"))

reproductive_rate = int(input("Number of PAIRS OF OFFSPRING per mating pair:"))

while (reproductive_rate > 5 or reproductive_rate < 0):
    reproductive_rate = int(input("PAIRS OF OFFSPRING must be represented by an integer that is not greater than 5:"))

babies = 1
previous_adults = 0
adults = 0

for x in range(1,months):
    adults = babies + adults
    babies = previous_adults*reproductive_rate
    previous_adults = adults

total_pairs_of_rabbits = (adults+babies)
print(total_pairs_of_rabbits)