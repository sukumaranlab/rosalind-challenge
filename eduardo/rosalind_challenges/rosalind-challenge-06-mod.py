dna_original = "GAGCCTACTAACGGGAT"
dna_test = "GAGCCTACTAACGGGAT"
dna_mutated = "CATCGTAATGACGGCCT"

#---------------------------------------------------------------------------------

def hamming_distance(dna1, dna2):
    """
    This calculates the Hamming distance for a DNA string using an iterative
    algorithm.

    The Hamming distance is ...

    This program will check to see if the input string is exactly identical to
    a known input string.

    Parameters:
    -----------
    dna1    : sequence of characters
        A string or some other sequence representing a DNA sequence.

    Returns
    --------
    d : (int)
        The Hamming distance.

    """
    hamm_distance = 0
    if len(dna1) != len(dna2):
        raise ValueError("DNA strings are of unequal length: {} vs {}".format(dna1, dna2))
    # for n in range(len(dna_original)):
    for ch1, ch2 in zip(dna1, dna2):
        if ch1 != ch2:
            hamm_distance += 1
    return(hamm_distance)

# print(mutation_iterative.__doc__)


def test():
    test_data = [
        ("CAGCCTACTAACGGGAT", "CATCGTAATGACGGCCT", 6),
        ("GAGCCTACTAACGGGAT", "CATCGTAATGACGGCCT", 7),
        ("GAGCCTACTAACGGGAT", "GAGCCTACTAACGGGAT", 0),
    ]

    # for (dna1, dna2, expected_result) in test_data:
    for test_case in test_data:
        dna1 = test_case[0]
        dna2 = test_case[1]
        expected_result = test_case[2]
        d = hamming_distance(dna1, dna2)
        print("- {} vs {}:".format(dna1, dna2))
        if d == expected_result:
            print("  {} [correct]".format(d))
        else:
            print("  {} [WRONG: expecting: {}]".format(d, expected_result))

test()



# #--------------------------------------------------------------------------------

# def mutation_recursive(count=0):
#     if dna_original[count] != dna_mutated[count]:
#         result = 1
#     else:
#         result = 0

#     if count == (len(dna_original)-1):
#         return(result)
#     else:
#         return(result + mutation_recursive(count+1))

# print(mutation_iterative())
# print(mutation_recursive())
