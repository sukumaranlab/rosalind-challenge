DNA = ("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC")

A_count = 0
G_count = 0
C_count = 0
T_count = 0

for char in DNA:
    if char == "A":
        A_count = A_count+1
    if char == "G":
        G_count = G_count+1
    if char == "C":
        C_count = C_count+1
    if char == "T":
        T_count = T_count+1
    
print(A_count, G_count, C_count, T_count)