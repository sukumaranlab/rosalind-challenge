dna_original = "GAGCCTACTAACGGGAT"
dna_test = "GAGCCTACTAACGGGAT"
dna_mutated = "CATCGTAATGACGGCCT"

#---------------------------------------------------------------------------------

def mutation_iterative():
    hamm_distance = 0
    for n in range(len(dna_original)):
        if dna_original[n] != dna_test[n]:
            print("ERROR IN CONTROL")
            break
        elif dna_original[n] != dna_mutated[n]:
            hamm_distance += 1
    return(hamm_distance)
            

#--------------------------------------------------------------------------------

def mutation_recursive(count):
    if dna_original[count] != dna_mutated[count]:
        result = 1
    else:
        result = 0

    if count == (len(dna_original)-1):
        return(result)
    else:
        return(result + mutation_recursive(count+1))

print(mutation_iterative())
print(mutation_recursive(0))
