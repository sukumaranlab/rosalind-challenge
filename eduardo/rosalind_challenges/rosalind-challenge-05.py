DNA_FILENAME = "file_DNA.txt"

def load_fasta():
    inFile = open(DNA_FILENAME, 'r')
    data = inFile.readlines()
    print(data)
    return(data)

def fasta_reader(fasta):
    numbers = "1234567890"
    gc_name = ""
    gc_count = 0
    gc_content = 0
    temp_gc_content = 0
    temp_gc_name_list = []
    nucleo_count = 0

    for line in fasta:
        if line[0] == ">":
            if gc_content < temp_gc_content:
                gc_content = temp_gc_content
                gc_name = ''.join(str(elem) for elem in temp_gc_name_list)

            temp_gc_content = 0
            nucleo_count = 0
            gc_count = 0
            temp_gc_content = 0
            temp_gc_name_list = []

            for char in line:
                if char in numbers:
                    temp_gc_name_list.append(char)
        else:
            for char in line:
                if char == "G" or char == "C":
                    gc_count += 1
                    nucleo_count += 1
                    print(gc_count, nucleo_count)
                if char == "A" or char == "T":
                    nucleo_count += 1
                temp_gc_content = gc_count/nucleo_count * 100

    if gc_content < temp_gc_content:
                gc_content = temp_gc_content
                gc_name = ''.join(str(elem) for elem in temp_gc_name_list)
    
    print(" Rosalind_",gc_name,"\n",gc_content)

fasta = load_fasta()
fasta_reader(fasta)