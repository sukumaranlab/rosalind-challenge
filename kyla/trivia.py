def ask_question(question, answer, explain_right, explain_wrong):
    ans = input(question + "")
    correct = 0
    if answer.lower() == ans.lower():
        print("You are correct! " + explain_right)
        correct = 1
    else:
        print("Sorry, that's wrong! " + explain_right)
    input("Press ENTER for your next question ")
    # value that comes back from the function
    return correct


def calcu_grade(correct):
    grade = (correct / 10) * 100
    print("This is how many you got right! " + str(grade))
    if(grade >= 90):
        letterGrade = "A"
    if(grade >= 80):
        letterGrade = "B"
    if(grade >= 70):
        letterGrade = "C"
    if(grade <= 69):
        letterGrade = "F"
    print()
    print("This is your letter grade! " + letterGrade)
    print()


def main():
        # intro message
    print()
    input("Welcome to Science Trivia! A game with the same exact questions and even similar answers! ")
    name = input("Before we begin, what is your name? ")
    print()
    print("Hi " + name + "! You're ready to begin! Press ENTER for the first question ")
    input()
    # start with 0 right and wrong
    correct = 0
    incorrect = 0
    # take existing number of correct and add what's coming back from function
    correct += ask_question(
        "What is the biggest planet in our solar system? ",
        "jupiter",
        "Jupiter is more than twice the size of all other planets combined!",
        "Jupiter is the biggest planet and is more than twice the size of all other planets combined!")
    print()
    # question 2
    correct += ask_question(
        "What is the first element in the periodic table? ",
        "hydrogen",
        "Hydrogen is the first element and is also the most common element on Earth!",
        "Hydrogen is the first element on the periodic table. It is also the most common element on Earth!")
    print()
    # question 3
    correct += ask_question(
        "What is the largest animal on Earth? ",
        "blue whale",
        "The blue whale animal we have ever known to exist on Earth!",
        "The blue whale is the largest animal known to exist on Earth!",)
    print()
    # question 4
    correct += ask_question(
        "What was the first planet discovered using a telescope? ",
        "uranus",
        "Uranus was discovered in 1781 by Sir William Herschel.",
        "Uranus was the first planet discovered in 1781 by Sir William Herschel.",)
    print()
    # question 5
    correct += ask_question(
        "What natural phenomena is measured by the Richter scale? ",
        "earthquakes",
        "The largest earthquake recorded in California was measured at 7.9 in 1857.",
        "The Richter scale measures earthquakes!")
    print()
    # question 6
    correct += ask_question(
        "What is the outermost layer of Earth called? ",
        "crust",
        "The crust is also the thinnest layer!",
        "The crust is the outermost and thinnest later!",)
    print()
    # question 7
    correct += ask_question(
        "The average human body contains how many pints of blood? ",
        "10",
        "",
        "There are 10 pints of blood in the average human body!",)
    print()
    # question 8
    correct += ask_question(
        "What is the world's largest ocean? ",
        "pacific ocean",
        "",
        "The correct answer is the Pacific Ocean!",)
    print()
    # question 9
    correct += ask_question(
        "What is the chemical formula for ozone? ",
        "O3",
        "",
        "The correct answer is: O3",)
    print()
    # question 10
    correct += ask_question(
        "What planet in our solar system has the longest day? ",
        "venus",
        "One day on Venus is equal to 116 days on Earth!",
        "Venus has the longest day, equal to 116 days on Earth!",)
    print()
    # number of incorrect calculated
    incorrect = 10 - correct
    print("You're finished, " + str(name) + "! Here is your final score")
    print()
    print("Correct " + str(correct))
    print()
    print("Incorrect " + str(incorrect))
    print()
    calcu_grade(correct)


main()
