class dog:
    def __init__(self, name, age, breed):
        self.name = name
        self.age = age
        self.breed = breed

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def get_breed(self):
        return self.breed


class Hello:
    def __init__(self, age, breed):
        self.name = name
        self.age = age
        self.breed = breed

    def show(self):
        print(f"This is {self.name} and they're {self.age} years old! ")

    def speak(self):
        print("...")


class SmallDog(Hello):
    def speak(self):
        print("Bark! Bark!")


class BigDog(Hello):
    def speak(self):
        print("Woof! Woof!")


labrador = dog("Mookie", 4, "Labrador Retriever")
shepherd = dog("Tasha", 7, "German Shepherd")
chihuahua = dog("Ruby", 12, "Chihuahua")
husky = dog("Kato", 3, "Husky")
pitbull = dog("Mojo", 11, "Pitbull")
yorkie = dog("Holly", 6, "Yorkie")
dalmatian = dog("Grace", 9, "Dalmatian")


def main():
    input("Welcome to the San Diego Dog Park! There are many dogs to visit here today! ")
    print()
    print("Here are the dogs who are at the dog park today!")
    dog_list = ["Mookie", "Tasha", "Ruby", "Kato", "Mojo", "Holly", "Grace"]
    print(dog_list)
    print()
    input("Which dog would you like to visit first?")


main()
