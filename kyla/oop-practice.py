class employee:
    def __init__(self, name, number, title):
        self.name = name
        self.number = number
        self.title = title

    def speak(self, sound):
        return f"(self.name) says (sound)"


class

james = employee("James Toft", 6205, "Custodial")
erica = employee("Erica Galliger", 1175, "CEO")
liana = employee("Liana Ramon", 8237, "Marketing Manager")
oliver = employee("Oliver Falla", 9156, "IT Department")
jaden = employee("Jaden Lilburn", 4274, "CFO")
angelina = employee("Angelina Garcia", 1290, "COO")
tia = employee("Tia Anderson", 7745, "Manager")

james.speak(
    "It's ok, but these people are disgusting and I wish I was paid more.")
erica.speak(
    "I am the boss. No one is below me. I love it. But I want more vacation time.")
liana.speak("Most of the people who are a part of my team are underqualified for their positions. I feel like I'm babysitting.")
oliver.speak(
    "Many of them do not know how to properly work their computers so I'm always busy doing something.")
jaden.speak("Don't tell anyone but this company is on its way to bankruptcy.")
angelina.speak(
    "I'm preparing for this company to go bankrupt, it's very stressful.")
tia.speak("I love my job! I love everything about my job!")


def main():
    print()
    print("You now have access to Bark and Bailey's Dog Toy Company's Employee List. ")
    print()
    name = input("Please enter your name and four digit employee ID. ")
    print()
    print("Welcome back " + name + ". You now have access to the employee list.")
    print("")


main()


# edits
class Park:
    def __init__(self, name, max_dogs):
        self.name = name
        self.max_dogs = max_dogs
        self.dogs = []

    def add_dog(self, dog):
        if len(self.dogs) < self.max_dogs:
            self.dogs.append(dog)
            return True
        return False

    def get_average_age(self):
        value = 0
        for dog in self.dogs:
            value += dog.get_age()

        return value / len(self.dogs)
