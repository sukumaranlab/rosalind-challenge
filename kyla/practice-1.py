# practicing string, functions, conditionals
name = input("Hello. What is your name? ")
# whatever name is inserted will be printed out in response
print("It's nice to meet you " + name)
# now we ask for age of the person
age = input("How old are you? ")
age = int(age)
# setting up conditional
if age < 90 and age > 55:
    print("Wow, you're old")
elif age > 90:
    print("RIP to your life")
else:
    print("How youthful!")
# do they wish to continue? yes or no?
answer = input("Would you like to know what year it will be when you're 100? ")
# next is a function that shows how old they will be in 100 years
if answer == "yes":
    # first needed is 'year' function
    year = str((2020 - age) + 100)
    print(name + ", you will be 100 years old in " + year)
if answer == "no":
    print("Alright. Well, goodbye.")
