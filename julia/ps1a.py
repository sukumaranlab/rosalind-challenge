annual_salary = float(input('Enter yout annual salary: '))

monthly_salary = annual_salary/12

portion_saved = float(input('Enter the percent of your salary to save, as a decimal:'))

total_cost = float(input('Enter the cost of your dream home: '))

portion_down_payment = 0.25

down_payment = total_cost*portion_down_payment

r = 0.04
investment_revenue = monthly_salary*r/12


x = portion_saved*monthly_salary+investment_revenue

y = down_payment/x

print ('Number of months:', round (y, 1))
