def function_to_calc(annual_salary, portion_saved, portion_down_payment, investment_interest_rate, total_cost):
    monthly_salary = annual_salary/12
    number_of_months = (total_cost*portion_down_payment/((monthly_salary)*portion_saved + (monthly_salary)*(investment_interest_rate/12)))
    print ('Number of months:', round (number_of_months, 1))    

def run_function_with_user_input(annual_salary, portion_saved, portion_down_payment, investment_interest_rate, total_cost):
    return function_to_calc(annual_salary, portion_saved, portion_down_payment, investment_interest_rate, total_cost)


run_function_with_user_input(100000, 0.1, 0.25, 0.04, 1000000)    
run_function_with_user_input(80067.23, 0.17, 0.37, 0.01, 1090999)

def run_function_with_test_inputs(annual_salary, portion_saved, portion_down_payment, investment_interest_rate, total_cost):       
    monthly_salary = annual_salary/12
    down_payment = total_cost*portion_down_payment
    investment_revenue = monthly_salary*investment_interest_rate/12
    money_for_down_payment_per_month = (portion_saved*monthly_salary)+investment_revenue
    months_to_pay = down_payment/money_for_down_payment_per_month
    print ('Test Input:', months_to_pay)


run_function_with_test_inputs(100000, 0.1, 0.25, 0.04, 1000000)
run_function_with_test_inputs(80067.23, 0.17, 0.37, 0.01, 1090999)

def function_with_raise(annual_salary_before_raise, raise_rate, raise_percent, years_of_work): 
    # raise_rate = frequnecy one gets a raise (yearly =1), raise_percent = raises are normally given to employees in the U.S. as a precentage of their current salary (avrg. 3-5%)
    salary_with_raise = annual_salary_before_raise*(raise_rate + (raise_percent/raise_rate))**(raise_rate*years_of_work)
    print ('Your salary after your raise is now', salary_with_raise, 'for', years_of_work, 'years of work.')


    function_with_raise(100000, 1, 0.03, 2)
    function_to_calc (106090, 0.1, 0.25, 0.04, 1000000)