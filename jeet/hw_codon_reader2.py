
def break_into_triplets(DNA_list):
    """
    what I expect as input:
    -   DNA_list to be a sequence of nucleotide characters
        -   characters must be uppercase
        -   no invalid characters
            -   anything but ACGT
        -   length must be divisible by 3
            -   if not, error is raised
            -   if not, warning is given and extra characters
        -   the original list will not be changed
    what I will give you in return:
    -   DNA_codon
        -   this will be a list of strings, where each string element consists
            of three characters in the same order as the input sequence
    """
    print("Input: {}".format(DNA_list))
    DNA_codon = []
    for loop_idx, ch in enumerate(DNA_list[::3]):
        seq_idx = loop_idx * 3
        # print("loop_idx: {}, seq_idx: {}, ch: {}".format(loop_idx, seq_idx, ch))
        codon = DNA_list[seq_idx:seq_idx+3]
        DNA_codon.append(codon)
    return DNA_codon


DNA_list = "ATGCGG"
expected = ["ATG", "CGG"]

DNA_list = "ATGCGGC"
expected = ["ATG", "CGG", "C"]

DNA_list = "ATGCGGTTCCAGTTAG"
expected = ['ATG', 'CGG', 'TTC', 'CAG', 'TTA', 'G']

DNA_codon2 = break_into_triplets(DNA_list)
if DNA_codon2 == expected:
    print("OK")
else:
    print("FAIL")

