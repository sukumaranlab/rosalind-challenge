DNA_list = list("ATGCGGTTCCAGTTAG")
expected = ['ATG', 'CGG', 'TTC', 'CAG', 'TTA', 'G']

DNA_list = "ATGCGG"
expected = ["ATG", "CGG"]

DNA_list = "ATGCGGC"
expected = ["ATG", "CGG", "C"]

DNA_list = "ATGCGGCT"
expected = ["ATG", "CGG", "CT"]

DNA_codon = list("")

n = 0
# what I expect as input:
#   -   DNA_list to be a sequence of nucleotide characters
#       -   characters must be uppercase
#       -   no invalid characters
#           -   anything but ACGT
#       -   length must be divisible by 3
#           -   if not, error is raised
#           -   if not, warning is given and extra characters
#       -   the original list will not be changed
# what I will give you in return:
#   -   DNA_codon
#       -   this will be a list of strings, where each string element consists
#           of three characters in the same order as the input sequence
while True:
    try:
        codon = DNA_list[n] + DNA_list[n + 1] + DNA_list[n + 2]
        DNA_codon.append(codon)
        n += 3
    except IndexError:
        try:
            codon = DNA_list[n] + DNA_list[n + 1]
            DNA_codon.append(codon)
        except IndexError:
            try:
                codon = DNA_list[n]
                DNA_codon.append(codon)
            except IndexError:
                pass
        break

print(DNA_codon)
if DNA_codon == expected:
    print("OK")
else:
    print("FAIL")
