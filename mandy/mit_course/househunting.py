current_savings = 0


def run_function_with_user_input(user_message, value_conversion):
    while True:
        try:
            user_input = input(user_message)
            user_validated_input = value_conversion(user_input)
            break
        except ValueError as e:
            print ("'{}' is not a valid value" .format(user_input))
    return user_validated_input


def function_months(months):
   monthly_saving = (annual_salary/12) * portion_saved
   portion_down_payment = 0.25
   down_payment = total_cost * portion_down_payment
   months = down_payment/(current_savings + monthly_saving)
   print ("Number of months: {}".format(months))
   return months 

print ("Hi, it is nice to meet you. This program will help you know how many months it will take to save for the down payment on your dream home!")
annual_salary = run_function_with_user_input("What is your starting annual salary? ", float)
portion_saved = run_function_with_user_input("Enter portion of your salary you are planning on saving as a decimal: ", float)
total_cost = run_function_with_user_input("What is the cost of your dream home? ", float)

months= 0
x = function_months(months)

if x < 24:
  print("You are SO close to the downpayment on your dream home!")
elif x < 120:
  print("Still have a few years until you can get your dream home")
elif x > 120:
  print("Maybe look for a new dream home :/")

