#!/usr/bin/env python2
# -*- coding: utf-8 -*-
def is_even (x):
    """
    Input: x, an number
    Returns True if it is a number, otherwise False
    """
    remainder= x % 2
    return remainder == 0

print ("All numbers between 0 and 15: even and odd")
for x in range (15):
    if is_even (x):
        print (x, "even")
    else:
        print (x, "odd")
        